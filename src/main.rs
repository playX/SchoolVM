#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(unused_imports)]
#![allow(dead_code)]
#![allow(unused_parens)]
#![allow(unused_mut)]
#[macro_use]
extern crate log;
#[macro_use]extern crate hmap;

use std::collections::HashMap;
use vm::bytecode::Opcode;
use vm::bytecode::*;
use vm::frame::Frame;
use vm::VM;

mod vm;

fn main() {
    use self::Opcode::*;
    use self::Value::*;

    let _fun = Fun(
        vec![
            JUMP(3),
            LOAD_FAST("n".to_string()),
            RETURN,
            NOP,
            LOAD_FAST("n".to_string()),
            PUSH(Int(1)),
            EQ,
            JUMP_IF_TRUE(1),
            PUSH(Int(1)),
            LOAD_FAST("n".to_string()),
            SUB,
            LOAD_FAST("fac".to_string()),
            CALL(1),
            LOAD_FAST("n".to_string()),
            MUL,
            LOAD_FAST("print".to_string()),
            CALL(1),
            RETURN,
        ],
        vec!["n".to_string()],
    );

    let test_class = Dict(hmap!(
        "greet".to_string() => Box::new(Value::Fun(vec![
            PUSH(Value::String("Hello, ".to_string())),

            LOAD_FAST("name".to_string()),
            PUSH(Value::String("!".to_string())),


            LOAD_FAST("print".to_string()),
            CALL(3),
            EXIT,
            ],
            vec![]
        )),
        "name".to_string() => Box::new(Value::String(std::string::String::new()))
    ));

    use std::any::*;


    let code: Vec<_> = vec![
        PUSH(RustFun(print)),
        STORE_FAST("print".to_string()),
        PUSH(test_class),
        PUSH(String("Greeter".to_string())),
        BUILD_CLASS,
        STORE_FAST("Greeter".to_string()),


        PUSH(String("Adel".to_string())),
        LOAD_FAST("Greeter".to_string()),
        SET_ATTR("name".to_string()),
        STORE_FAST("Greeter".to_string()),

        LOAD_FAST("Greeter".to_string()),
        CALL_ATTR("greet".to_string(),0),

        EXIT,
    ];
    let frame = Frame::new(code, HashMap::new());

    let mut vm = VM::new();
    let result = vm.run_frame(frame).unwrap();
    println!("{:?}", result);
}


pub fn print(_: &mut VM,args: Vec<Value>) -> Option<Value> {
    for value in args.iter() {
        print!("{}",value);
    }

    println!();
    if args.first().is_some() {
        let res = args.first().unwrap().clone();
        return Some(res);
    } else {
        return Some(Value::Null);
    }
}