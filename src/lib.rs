#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(unused_imports)]
#![allow(dead_code)]
#![allow(unused_parens)]
#![allow(unused_mut)]
#[macro_use]extern crate log;

use vm::VM;
use vm::frame::Frame;
use vm::bytecode::Opcode;
use vm::bytecode::*;
use std::collections::HashMap;


use std::alloc::{System,Layout,GlobalAlloc};

struct Allocator;

unsafe impl GlobalAlloc for Allocator {
    unsafe fn alloc(&self,l: Layout) -> *mut u8 {
        System.alloc(l)
    }   

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        System.dealloc(ptr, layout)
    }
}

#[global_allocator]
static GLOBAL: Allocator = Allocator;

pub mod vm;


