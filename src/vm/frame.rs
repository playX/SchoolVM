extern crate rand;

use self::rand::{Rng,thread_rng};

use super::bytecode::*;

use std::collections::HashMap;

#[derive(Clone,Debug)]
pub struct Frame {
    pub code: Vec<Opcode>,
    pub locals: HashMap<String,Value>,
    pub labels: HashMap<String,usize>,
    pub stack: Vec<Value>,
    pub pc: usize,
    pub id: usize,
}

impl Frame {
    pub fn new(code: Vec<Opcode>,globals: HashMap<String,Value>) -> Frame {
        let locals = globals;
        let mut rng = thread_rng();
        let id: usize = rng.gen();
        Frame {
            code: code,
            locals: locals,
            pc: 0,
            id: id,
            labels: HashMap::new(),
            stack: Vec::new(),
        }
    }

    pub fn fetch_opcode(&mut self) -> Opcode {
        let op = self.code[self.pc].clone();
        self.pc += 1;
        return op;
    }

    pub fn push(&mut self,v: Value) {
        self.stack.push(v);
    }

    pub fn new_label_here(&mut self,name: String) {
        self.labels.insert(name,self.pc.clone());
    }

    pub fn new_label(&mut self,name: String,p: usize) {
        self.labels.insert(name,p);
    }

    pub fn pop(&mut self) -> Result<Value,String> {
        let result = self.stack.pop();
        if result.is_some() {
            return Ok(result.unwrap());
        } else {
            println!("ops: {:?}",self.code);
            println!("{:?}",self.code[self.pc]);
            return Err(format!("[ERROR] No value to pop in frame with id: {}",self.id));
        }
    }


    pub fn emit(&mut self,op:Opcode) {
        self.code.push(op);
    }


}

