use super::bytecode::*;
use super::frame::*;

use std::string::String as RString;



pub struct VM {
    pub frames: Vec<Frame>,
    debug: bool,
}



use self::Value::*;

impl VM {
    pub fn new() -> VM {
        let frames = vec![];
        let log;
        if cfg!(debug_assertions) {
            log = true;
        } else {
            log = false;
        }
        VM {
            frames,
            debug: log,
        }
    }


    pub fn run_frame(&mut self,f: Frame) -> Result<Value,RString> {



        self.frames.push(f);

        let value = loop {
            let result = self.execute_opcode();
            match result {
                None => {},
                Some(Ok(value)) => {
                    break Ok(value)
                },
                Some(Err(e)) => {

                    break Err(e)
                }
            }
        };

        self.frames.pop();

        return value;

    }

    pub fn current_frame(&self) -> Frame {
        self.frames.last().expect("No frames in stack").clone()
    }
    pub fn current_frame_mut(&mut self) -> &mut Frame {
        self.frames.last_mut().expect("No frames in stack")
    }

    pub fn pop(&mut self) -> Value {
        let res = self.current_frame_mut().pop();

        if res.is_err() {

            res.unwrap()
        } else {
            res.unwrap()
        }
    }

    pub fn push(&mut self,v: Value) {
        self.current_frame_mut().push(v);
    }

    pub fn execute_opcode(&mut self) -> Option<Result<Value,RString>> {
        let opcode = self.current_frame_mut().fetch_opcode();

        use self::Opcode::*;

        match &opcode {
            PUSH(v) => {

                self.push(v.clone());
                None
            },
            POP => {
                self.pop();
                None
            },
            STORE_FAST(ref name) => {
                let value = self.pop();
                self.current_frame_mut().locals.insert(name.clone(),value);
                None
            }
            LOAD_FAST(ref name) => {

                let v = self.current_frame_mut().clone();
                let v = v.locals.get(name).expect(&format!("Variable {} not found",name));
                self.push(v.clone());
                None
            }

            DELETE(ref name) => {
                self.current_frame_mut().locals.remove(name);
                None
            }

            NOP => {None},
            JUMP(pc) => {
                self.current_frame_mut().pc = *pc;
                None
            }

            CALL_ATTR(ref name,argc) => {
                let (_name,mut dict) = match self.pop() {
                    Class(_name,dict) => {
                        let dict = match *dict {
                            Dict(hashmap) => hashmap,
                            v => return Some(Err(format!("Expected Dict value,found: {:?}",v))),
                        };

                        (_name,dict)
                    }
                    v => return Some(Err(format!("Expected Dict value,found: {:?}",v))),

                };

                let mut globals = dict.clone();
                let val = &**dict.get(name).unwrap();
                use std::collections::HashMap;
                match val {
                    Fun(code,args) => {
                        if argc < &args.len() {
                            return Some(Err(format!("Expected {} argument(s),found {}",args.len(),argc)));
                        }

                        let mut values = vec![];
                        for _ in 0..*argc {
                            values.push(self.pop());
                        }
                        values.reverse();

                        for (value,name) in values.iter().zip(args) {
                            globals.insert(name.to_string(),Box::new(value.clone()));
                        }
                        let mut converted = HashMap::new();

                        for (name,val) in globals {
                            converted.insert(name,*val);
                        }
                        let locals = self.current_frame();
                        let mut map = HashMap::new();
                        let locals = locals.locals.clone();
                        for (c,l) in converted.iter().zip(locals) {
                            if c.0.clone() == l.0 {
                                map.insert(c.0.clone(),c.1.clone());
                            } else {
                                map.insert(c.0.clone(),c.1.clone());
                                map.insert(l.0,l.1);
                            }
                        }
                        let frame = Frame::new(code.to_vec(),map);
                        let result = self.run_frame(frame);

                        if result.is_ok() {
                            self.push(result.unwrap())
                        } else {
                            result.unwrap();
                        }

                    },
                    RustFun(function) => {
                        let mut values = vec![];
                        for _ in 0..*argc {
                            values.push(self.pop());
                        }

                        values.reverse();
                        let result = function(self,values);

                        if result.is_some() {
                            self.push(result.unwrap());
                        }
                    }

                    _ => unimplemented!(),
                }

                None
            }

            CALL(argc) => {
                let fptr = self.pop();
                if fptr.is_fun() {
                    let tup = match fptr {
                        Fun(ops,args) => (ops,args),
                        _ => panic!("Expected fun value"),
                    };

                    if argc < &tup.1.len() {
                        return Some(Err(format!("Expected {} argument(s),found {}",tup.1.len(),argc)));
                    }

                    let globals = self.current_frame();

                    let mut values = vec![];

                    for _ in 0..*argc {
                        values.push(self.pop());
                    }
                    values.reverse();




                    let mut globals = globals.locals.clone();
                    for (value,name) in values.iter().zip(tup.1) {
                        globals.insert(name,value.clone());
                    }
                    let frame = Frame::new(tup.0,globals);

                    let result = self.run_frame(frame);

                    if result.is_ok() {
                        self.push(result.unwrap())
                    } else {
                        result.unwrap();
                    }
                } else if fptr.is_rust_fun() {

                    let function = match fptr {
                        RustFun(f) => f,
                        _ => panic!("Expected rust function"),
                    };

                    let mut values = vec![];

                    for _ in 0..*argc {
                        values.push(self.pop());
                    }

                    values.reverse();
                    let result = function(self,values);

                    if result.is_some() {
                        self.push(result.unwrap());
                    }
                } else {
                    return Some(Err("Expected function value".to_string()));
                }



                None
            }

            JUMP_IF_TRUE(pc) => {

                let b = self.pop();



                let bool = match b {
                    Int(v) => v > 0,
                    Float(V) => V > 0.0,
                    Bool(n) => n.num() == 1,
                    v => return Some(Err(format!("Expected Int,Bool,Float found: {:?}",v))),
                };

                if bool {
                    self.current_frame_mut().pc = *pc;
                }
                None
            }

            JUMP_IF_FALSE(pc) => {
                let b = self.pop();
                let bool = match b {
                    Int(v) => v <= 0,
                    Float(V) => V <= 0.0,
                    Bool(n) => n.num() == 0,
                    v => return Some(Err(format!("Expected Int,Bool,Float found: {:?}",v))),
                };

                if bool == false {
                    self.current_frame_mut().pc = *pc;
                }

                None
            }

            RETURN => {
                let ret_val = self.pop();
                return Some(Ok(ret_val));
            }
            EXIT => {
                return Some(Ok(Null));
            }

            UnaryMinus => {
                let v = self.pop();
                match v {
                    Int(i) => self.push(Int(-i)),
                    Float(f) => self.push(Float(-f)),
                    val => return Some(Err(format!("Cannot apply UnaryMinus to {:?}",val))),
                }
                None
            }
            Not => {
                let v = self.pop();
                match v {
                    Int(i) => self.push(Int(!i)),
                    //Float(f) => self.push(!f),
                    val => return Some(Err(format!("Cannot apply UnaryNot to {:?}",val))),
                }
                None
            }
            I2F => {
                let int = match self.pop() {
                    Int(i) => i,
                    _ => return Some(Err("Expected Int value in OP(I2F)".to_string())),
                };

                self.push(Float(int as f64));
                None
            }

            F2I => {
                let float = match self.pop() {
                    Float(f) => f,
                    _ => return Some(Err("Expected Float value in OP(F2I)".to_string())),
                };
                self.push(Int(float as i64));
                None
            }

            EQ => {
                let v1 = self.pop();
                let v2 = self.pop();
                self.push(Bool(N1::new((v1 == v2) as usize)));
                None
            }
            GT => {

                let v1 = self.pop();
                let v2 = self.pop();
                let res = N1::new((v1 > v2) as usize);

                self.push(Bool(res));
                None
            }
            GE => {
                let v1 = self.pop();
                let v2 = self.pop();
                self.push(Bool(N1::new((v1 >= v2) as usize)));
                None
            }
            LT => {
                let v1 = self.pop();
                let v2 = self.pop();
                self.push(Bool(N1::new((v1 < v2) as usize)));
                None
            }
            LE => {
                let v1 = self.pop();
                let v2 = self.pop();
                self.push(Bool(N1::new((v1 <= v2) as usize)));
                None
            }


            ADD => {
                self._add();
                None
            }
            MUL => {
                self._mul();
                None
            }
            DIV => {
                self._div();
                None
            }
            SUB => {
                self._sub();
                None
            }



            XOR => {
                let v1 = self.pop();
                let v2 = self.pop();

                let result = match (v2,v1) {
                    (Int(i),Int(i2)) => i ^ i2,
                    (val,val2) => return Some(Err(format!("Cannot XOR {:?} {:?}",val,val2)))
                };

                self.push(Int(result));

                None
            }

            AND => {
                let v1 = self.pop();
                let v2 = self.pop();

                match (v2,v1) {
                    (Int(i),Int(i2)) => self.push(Int(i & i2)),
                    (Int(i),Float(f)) => self.push(Int(i & f as i64)),
                    (Float(f),Int(i)) => self.push(Int(f as i64 & i)),
                    (Float(f),Float(f2)) => self.push(Int(f as i64 & f2 as i64)),
                    (Bool(n1),Bool(n2)) => self.push(Bool(N1::new((n1.bool() & n2.bool()) as usize))),
                    (val,val2) => return Some(Err(format!("Cannot apply AND to {:?} and {:?}",val,val2))),
                }

                None
            }

            OR => {
                let v1 = self.pop();
                let v2 = self.pop();

                match (v2,v1) {
                    (Int(i),Int(i2)) => self.push(Int(i | i2)),
                    (Int(i),Float(f)) => self.push(Int(i | f as i64)),
                    (Float(f),Int(i)) => self.push(Int(f as i64 | i)),
                    (Float(f),Float(f2)) => self.push(Int(f as i64 | f2 as i64)),
                    (Bool(n1),Bool(n2)) => self.push(Bool(N1::new((n1.bool() | n2.bool()) as usize))),
                    (val,val2) => return Some(Err(format!("Cannot apply OR to {:?} and {:?}",val,val2))),
                }

                None
            }
            MOD => {
                let v1 = self.pop();
                let v2 = self.pop();

                match (v2,v1) {
                    (Int(i),Int(i2)) => self.push(Int(i % i2)),
                    (Int(i),Float(f)) => self.push(Float(i as f64 % f)),
                    (Float(f),Int(i)) => self.push(Float(f % i as f64)),
                    (Float(f),Float(f2)) => self.push(Float(f % f2)),
                    (val,val2) => return Some(Err(format!("Cannot apply MOD to {:?} ad {:?}",val,val2))),
                }

                None
            }

            POW => {
                let v1 = self.pop();
                let v2 = self.pop();

                match (v1,v2) {
                    (Int(i),Int(i2)) => self.push(Int(i.pow(i2 as u32))),
                    (Int(i),Float(f)) => self.push(Float((i.pow(f as u32) as f64))),
                    (Float(f),Int(i)) => self.push(Float(f.powf(i as f64))),
                    (Float(f),Float(f2)) => self.push(Float(f.powf(f2))),
                    (val,val2) => return Some(Err(format!("Cannot apply MOD to {:?} ad {:?}",val,val2))),
                }

                None
            }

            BUILD_LIST(len) => {
                let mut values = vec![];

                for _ in 0..*len {
                    values.push(self.pop());
                }
                values.reverse();
                self.push(Array(values));
                None
            }

            //На данный момент приходится создавать копию вектора и добавлять ее в стек, а уже затем изменять переменную
            STORE_SUBCR => {
                let idx = self.pop();
                let mut obj = self.pop();
                let value = self.pop();

                let idx = match idx {
                    Value::Int(id) => id,
                    v => return Some(Err(format!("Expected Int type; STORE_SUBCR idx,found: {:?}",v))),
                };

                let result =  match obj {
                    Value::Array(mut vec) => {
                        vec[idx as usize] = value;
                        vec
                    }
                    v => return Some(Err(format!("Expected Array value,found: {:?}",v))),
                };

                self.push(Array(result));

                None

            }

            SUBCR => {
                let mut a = self.pop();
                let id = self.pop();

                let id = match id {
                    Int(i) => i as usize,
                    _ => return Some(Err(format!("Indexing supported only for Int type"))),
                };

                let result = match a {
                    Value::Array(ref mut arr) => {
                        if arr.len() < id {
                            return Some(Err("Cannot move out of indexed content".to_string()));
                        } else {
                            unsafe {arr.get_unchecked(id).clone()}
                        }
                    },
                    Value::String(ref s) => {
                        let v: Vec<char> = s.chars().collect();

                        String(format!("{}",v[id]))
                    },
                    _ => return Some(Err("SUBCR implemented only for Array and String types".to_string())),
                };

                self.push(result);
                None
            }

            BUILD_CLASS => {
                let name = self.pop();
                let dict = self.pop();
                let name = match name {
                    String(ref s) => s,
                    v => return Some(Err(format!("Expected Value::String,found: {:?}",v))),
                };
                self.push(Class(name.clone(),Box::new(dict)));
                None
            }


            SET_ATTR(ref name) => {

                let (_name,mut dict) = match self.pop() {
                    Class(_name,dict) => {
                        let dict = match *dict {
                            Dict(hashmap) => hashmap,
                            v => return Some(Err(format!("Expected Dict value,found: {:?}",v))),
                        };

                        (_name,dict)
                    }
                    v => return Some(Err(format!("Expected Dict value,found: {:?}",v))),

                };

                dict.insert(name.clone(),Box::new(self.pop()));
                self.push(Class(_name,Box::new(Dict(dict))));
                None
            }

            LOAD_ATTR(ref name) => {
                let (_name,dict) = match self.pop() {
                    Class(name,dict) => {
                        let dict = match *dict {
                            Dict(hashmap) => {
                                println!("{:?}",hashmap);
                                hashmap},
                            v => return Some(Err(format!("Expected Dict value,found: {:?}",v))),
                        };

                        (name,dict)
                    },

                    v => return Some(Err(format!("Expected Dict value,found: {:?}",v))),
                };

                if let Some(val) = dict.get(name) {
                    let res = *val.clone();
                    self.push(res);
                } else {
                    return Some(Err(format!("Attribute {} not found in class {}",name,_name)));
                }
                None
            }


        }
    }

    //Сложение
    fn _add(&mut self) {
        let l = self.pop();
        let r = self.pop();

        match (r,l) {
            (Int(v1),Int(v2)) => {self.push(Int(v1 + v2))},
            (Float(v1),Float(v2)) => {self.push(Float(v1 + v2))},
            (String(ref s),String(ref s2)) => {self.push(String(format!("{}{}",s,s2)))},
            (Int(v1),Float(v2)) => {self.push(Float(v1 as f64 + v2))},
            (Float(v1),Int(v2)) => {self.push(Float(v1 + v2 as f64))},
            (Array(arr1),Array(arr2)) => {
                let mut ar = arr1.clone();

                ar.extend(arr2);
                self.push(Array(ar));
            }
            (v1,v2) => panic!(format!("Cannot add {:?} to {:?}",v1,v2)),

        }
    }
    //Вычитание
    fn _sub(&mut self) {
        let l = self.pop();
        let r = self.pop();

        match (r,l) {
            (Int(v1),Int(v2)) => {self.push(Int(v1 - v2))},
            (Float(v1),Float(v2)) => {self.push(Float(v1 - v2))},
            (Int(v1),Float(v2)) => {self.push(Float(v1 as f64 - v2))},
            (Float(v1),Int(v2)) => {self.push(Float(v1 - v2 as f64))},
            (v1,v2) => panic!(format!("Cannot sub {:?} and {:?}",v1,v2)),

        }
    }
    //Деление
    fn _div(&mut self) {
        let l = self.pop();
        let r = self.pop();

        match (r,l) {
            (Int(v1),Int(v2)) => {self.push(Int(v1 / v2))},
            (Float(v1),Float(v2)) => {self.push(Float(v1 / v2))},
            (Int(v1),Float(v2)) => {self.push(Float(v1 as f64 / v2))},
            (Float(v1),Int(v2)) => {self.push(Float(v1 / v2 as f64))},
            (v1,v2) => panic!(format!("Cannot divide {:?} by {:?}",v1,v2)),

        }
    }
    //Умножение
    fn _mul(&mut self) {
        let l = self.pop();
        let r = self.pop();

        match (r,l) {
            (Int(v1),Int(v2)) => {self.push(Int(v1 * v2))},
            (Float(v1),Float(v2)) => {self.push(Float(v1 * v2))},
            (Int(v1),Float(v2)) => {self.push(Float(v1 as f64 * v2))},
            (Float(v1),Int(v2)) => {self.push(Float(v1 * v2 as f64))},
            (v1,v2) => panic!(format!("Cannot multiply {:?} by {:?}",v1,v2)),

        }
    }




}