use std::collections::HashMap;
use std::cmp::Ordering;
use vm::VM;

#[derive(Clone)]
pub enum Value {
    Float(f64),
    Int(i64),
    String(String),
    Bool(N1),
    Fun(Vec<Opcode>,Vec<String>),
    RustFun(RustFunc),
    Dict(HashMap<String,Box<Value>>),
    Class(
        String,
        Box<Value>,
    ),
    Code(Vec<Opcode>),
    Array(Vec<Value>),
    Null,
}







impl Value {
    pub fn is_fun(&self) -> bool {
        match self {
            Value::Fun(_,_) => true,
            _ => false,
        }
    }

    pub fn is_rust_fun(&self) -> bool {
        match self {
            Value::RustFun(_) => true,
            _ => false,
        }
    }
}




use std::fmt;

impl fmt::Debug for Value {
    fn fmt(&self,f: &mut fmt::Formatter) -> fmt::Result {
        use self::Value::*;
        match self {
            Float(v) => write!(f,"Float({})",v),
            Int(v) => write!(f,"Int({})",v),
            String(ref v) => write!(f,"String({})",v),
            Bool(n) => {
                if n.num() == 1 {
                    write!(f,"Bool(true)")
                } else {
                    write!(f,"Bool(false)")
                }
            }
            Fun(_,_) => write!(f,"<function>"),
            RustFun(_) => write!(f,"<rust function>"),
            Null => write!(f,"Null"),
            Code(_) => write!(f,"Code"),
            Array(a) => write!(f,"Array({:?})",a),
            Class(name,_) => write!(f,"<class {}>",name),
            Dict(d) => write!(f,"{:?}",d),
        }
    }
}

impl fmt::Display for Value {
    fn fmt(&self,f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Value::Int(i) => write!(f,"{}",i),
            Value::Float(fl) => write!(f,"{}",fl),
            Value::Array(arr) => write!(f,"{:?}",arr),
            Value::String(ref s) => write!(f,"{}",s),
            Value::Bool(n) => {
                if n.num() == 1 {
                    write!(f,"true")
                } else {
                    write!(f,"false")
                }
            }
            v => write!(f,"{:?}",v)

        }
    }
}

pub type RustFunc = fn(&mut VM,Vec<Value>) -> Option<Value>;

impl PartialEq for Value {
    fn eq(&self,other: &Self) -> bool {
        use self::Value::*;
        match (self.clone(),other.clone()) {
            (Int(v1),Int(v2)) => return v1 == v2,
            (Float(v1),Float(v2)) => return v1 == v2,
            (Int(v1),Float(v2)) => return v1 as f64 == v2,
            (Float(v1),Int(v2)) => return v1 == v2 as f64,
            (Bool(b1),Bool(b2)) => {
                let v1 = if b1.num() == 1 {true} else {false};
                let v2 = if b2.num() == 1 {true} else {false};
                v1 == v2
            }
            (String(ref v1),String(ref v2)) => return v1 == v2,
            (Null,Null) => return true,
            (v,ve) => panic!(format!("Cannot compare {:?} and {:?}",v,ve)),
        }
    }
}

impl PartialOrd for Value {
    fn partial_cmp(&self,other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Value {
    fn cmp(&self,other: &Self) -> Ordering {
        use self::Value::*;
        match (self.clone(),other.clone()) {
            (Int(v1),Int(v2)) => return v1.cmp(&v2),
            (String(ref v1),String(ref v2)) => return v1.cmp(v2),
            (Float(v1),Float(v2)) => return v1.partial_cmp(&v2).unwrap(),
            (Float(v1),Int(v2)) => {let f = v2 as f64;return v1.partial_cmp(&f).unwrap()},
            (Bool(b1),Bool(b2)) => {
                let v1 = if b1.num() == 1 {true} else {false};
                let v2 = if b2.num() == 1 {true} else {false};
                v1.cmp(&v2)
            }
            (Int(v1),Float(v2)) => {
                let f = v1 as f64;
                return f.partial_cmp(&v2).unwrap();
            }
            (v,ve) => panic!(format!("Cannot compare {:?} and {:?}",v,ve)),
        }
    }
}

impl Eq for Value {}

#[derive(Clone,Debug,PartialEq,PartialOrd)]
pub enum N1 {
    N1 = 1,
    N0 = 0,
}

impl N1 {
    pub fn new(s: usize) -> N1 {
        match s {
            0 => N1::N0,
            _ => N1::N1
        }
    }

    pub fn num(&self) -> usize {
        match self {
            N1::N0 => 0,
            N1::N1 => 1,
        }
    }
    pub fn bool(&self) -> bool {
        match self {
            N1::N0 => false,
            N1::N1 => true,
        }
    }
}

#[derive(Clone,Debug,PartialEq,PartialOrd)]
pub enum Opcode {

    EQ,
    GT,
    GE,
    LT,
    LE,
    NOP,
    F2I,
    I2F,

    UnaryMinus,
    Not,

    RETURN,
    EXIT,
    STORE_FAST(String),
    LOAD_FAST(String),
    DELETE(String),


    PUSH(Value),
    POP,


    JUMP_IF_TRUE(usize),
    JUMP_IF_FALSE(usize),
    JUMP(usize),
    CALL(usize),



    ADD,
    SUB,
    MUL,
    DIV,

    BUILD_LIST(usize),

    STORE_SUBCR,
    SUBCR,

    LOAD_ATTR(String),
    SET_ATTR(String),
    CALL_ATTR(String,usize),
    BUILD_CLASS,

    XOR,
    AND,
    OR,
    MOD,

    POW,

}

impl From<i64> for Value {
    fn from(i: i64) -> Value {
        return Value::Int(i);
    }
}

impl From<i32> for Value {
    fn from(i: i32) -> Value {
        return Value::Int(i as i64);
    }
}

impl From<f32> for Value {
    fn from(f: f32) -> Value {
        return Value::Float(f as f64);
    }
}

impl From<f64> for Value {
    fn from(f: f64) -> Value {
        return Value::Float(f);
    }
}

impl From<&'static str> for Value {
    fn from(f: &'static str) -> Value {
        return Value::String(f.to_string());
    }
}

impl From<String> for Value {
    fn from(f: String) -> Value {
        return Value::String(f);
    }
}

impl From<bool> for Value {
    fn from(b: bool) -> Value {
        Value::Bool(N1::new(b as usize))
    }
}

impl From<Vec<Value>> for Value {
    fn from(v: Vec<Value>) -> Value {
        Value::Array(v)
    }
}

impl From<Vec<i64>> for Value {
    fn from(v: Vec<i64>) -> Value {
        let mut vec = vec![];

        for int in v.iter() {
            vec.push(Value::Int(*int));
        }

        Value::Array(vec)
    }
}
