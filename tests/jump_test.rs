extern crate school_vm;

use school_vm::vm::*;
use self::frame::*;
use self::bytecode::*;

#[test]
fn simple_while() {
    let mut vm = VM::new();

    let code = vec![
        Opcode::PUSH(Value::Int(0)),
        Opcode::STORE_FAST("i".into()),

        Opcode::NOP,
        Opcode::LOAD_FAST("i".into()),
        Opcode::PUSH(Value::Int(9)),
        Opcode::LT,
        Opcode::JUMP_IF_FALSE(12),
        Opcode::PUSH(Value::Int(1)),
        Opcode::LOAD_FAST("i".into()),
        Opcode::ADD,
        Opcode::STORE_FAST("i".into()),
        Opcode::JUMP(2),
        Opcode::LOAD_FAST("i".into()),
        Opcode::RETURN,
    ];

    use std::collections::HashMap;

    let frame = Frame::new(code,HashMap::new());

    let result = vm.run_frame(frame);

    assert_eq!(result.unwrap(),Value::Int(10));
}