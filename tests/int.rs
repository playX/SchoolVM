extern crate school_vm;

use school_vm::vm::*;
use self::frame::*;
use self::bytecode::*;

#[test]
fn add_test() {
    let mut vm = VM::new();

    let code = vec![
        Opcode::PUSH(Value::Int(2)),
        Opcode::PUSH(Value::Int(3)),
        Opcode::ADD,
        Opcode::RETURN,
    ];

    use std::collections::HashMap;
    let frame = Frame::new(code,HashMap::new());

    let result = vm.run_frame(frame);

    assert_eq!(result.unwrap(),Value::Int(5));
}

#[test]
fn sub_test() {
    let mut vm = VM::new();

    let code = vec![
        Opcode::PUSH(Value::Int(3)),
        Opcode::PUSH(Value::Int(2)),
        Opcode::SUB,
        Opcode::RETURN,
    ];

    use std::collections::HashMap;
    let frame = Frame::new(code,HashMap::new());

    let result = vm.run_frame(frame);

    assert_eq!(result.unwrap(),Value::Int(1));
}
#[test]
fn mod_test() {
    let mut vm = VM::new();

    let code = vec![
        Opcode::PUSH(Value::Int(3)),
        Opcode::PUSH(Value::Int(2)),
        Opcode::MOD,
        Opcode::RETURN,
    ];

    use std::collections::HashMap;
    let frame = Frame::new(code,HashMap::new());

    let result = vm.run_frame(frame);

    assert_eq!(result.unwrap(),Value::Int(3 % 2));
}

#[test]
fn mul_test() {
    let mut vm = VM::new();

    let code = vec![
        Opcode::PUSH(Value::Int(2)),
        Opcode::PUSH(Value::Int(3)),
        Opcode::MUL,
        Opcode::RETURN,
    ];

    use std::collections::HashMap;
    let frame = Frame::new(code,HashMap::new());

    let result = vm.run_frame(frame);

    assert_eq!(result.unwrap(),Value::Int(6));
}

#[test]
fn div_test() {
    let mut vm = VM::new();

    let code = vec![
        Opcode::PUSH(Value::Int(12)),
        Opcode::PUSH(Value::Int(2)),
        Opcode::DIV,
        Opcode::RETURN,
    ];

    use std::collections::HashMap;
    let frame = Frame::new(code,HashMap::new());

    let result = vm.run_frame(frame);

    assert_eq!(result.unwrap(),Value::Int(6));
}