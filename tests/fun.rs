extern crate school_vm;

use school_vm::vm::*;
use self::frame::*;
use self::bytecode::*;

use self::Opcode::*;
use self::Value::*;

#[test]
fn factorial() {
    let mut vm = VM::new();

    let fun = Fun(
        vec![
            PUSH(Int(1)), LOAD_FAST("n".into()), EQ, JUMP(16), PUSH(Int(1)), RETURN, JUMP(18), LOAD_FAST("n".into()), LOAD_FAST("n".into()), PUSH(Int(1)), SUB, LOAD_FAST("fac".into()), CALL(1), MUL, RETURN, JUMP(18), JUMP_IF_TRUE(4), JUMP(7)

        ],
        vec!["n".to_string()],
    );



    let code = vec![
        PUSH(RustFun(print)),
        STORE_FAST("print".to_string()),

        PUSH(fun),
        STORE_FAST("fac".to_string()),
        PUSH(Int(6)),
        LOAD_FAST("fac".to_string()),
        CALL(1),
        RETURN,
    ];
    use std::collections::HashMap;
    let frame = Frame::new(code,HashMap::new());
    let result = vm.run_frame(frame);







    assert_eq!(result.unwrap(),Value::Int(720));
}
fn print(_vm: &mut VM,args: Vec<Value>) -> Option<Value> {
    for arg in args.iter() {
        println!("{:?}",arg);
    }
    return Some(args.first().unwrap().clone());
}
