extern crate school_vm;

use school_vm::vm::*;
use self::frame::*;
use self::bytecode::*;

#[test]
fn add_test() {
    let mut vm = VM::new();

    let code = vec![
        Opcode::PUSH(Value::Float(2.5)),
        Opcode::PUSH(Value::Float(2.5)),
        Opcode::ADD,
        Opcode::RETURN,
    ];

    use std::collections::HashMap;
    let frame = Frame::new(code,HashMap::new());

    let result = vm.run_frame(frame);

    assert_eq!(result.unwrap(),Value::Float(5.0));
}

#[test]
fn sub_test() {
    let mut vm = VM::new();

    let code = vec![
        Opcode::PUSH(Value::Float(3.)),
        Opcode::PUSH(Value::Float(2.)),
        Opcode::SUB,
        Opcode::RETURN,
    ];

    use std::collections::HashMap;
    let frame = Frame::new(code,HashMap::new());

    let result = vm.run_frame(frame);

    assert_eq!(result.unwrap(),Value::Float(1.));
}
#[test]
fn mod_test() {
    let mut vm = VM::new();

    let code = vec![
        Opcode::PUSH(Value::Float(3.0)),
        Opcode::PUSH(Value::Float(2.0)),
        Opcode::MOD,
        Opcode::RETURN,
    ];

    use std::collections::HashMap;
    let frame = Frame::new(code,HashMap::new());

    let result = vm.run_frame(frame);

    assert_eq!(result.unwrap(),Value::Float(3.0 % 2.0));
}

#[test]
fn mul_test() {
    let mut vm = VM::new();

    let code = vec![
        Opcode::PUSH(Value::Float(2.0)),
        Opcode::PUSH(Value::Float(3.0)),
        Opcode::MUL,
        Opcode::RETURN,
    ];

    use std::collections::HashMap;
    let frame = Frame::new(code,HashMap::new());

    let result = vm.run_frame(frame);

    assert_eq!(result.unwrap(),Value::Float(6.0));
}

#[test]
fn div_test() {
    let mut vm = VM::new();

    let code = vec![
        Opcode::PUSH(Value::Float(12.0)),
        Opcode::PUSH(Value::Float(2.0)),
        Opcode::DIV,
        Opcode::RETURN,
    ];

    use std::collections::HashMap;
    let frame = Frame::new(code,HashMap::new());

    let result = vm.run_frame(frame);

    assert_eq!(result.unwrap(),Value::Float(6.0));
}